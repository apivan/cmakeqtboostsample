#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtQuick/QQuickView>

#include <boost/signals2/signal.hpp>

#include <memory>


namespace Ui {
  class MainWindow;
}

class MainWindow : public QMainWindow
{
  Q_OBJECT

public:
  explicit MainWindow(QWidget *parent = 0);
  ~MainWindow();


private slots:
   void OnQmlSignaled();

private:

  std::unique_ptr<Ui::MainWindow>  m_Ui;
  std::unique_ptr<QQuickView>      m_QmlView;
  boost::signals2::signal<void()>  m_BoostSignal;

};

#endif // MAINWINDOW_H
