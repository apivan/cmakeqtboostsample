#include <QtQuick/QQuickView>
#include <QQuickItem>
#define BOOST_THREAD_PROVIDES_FUTURE
#include <boost/thread.hpp>
#include <boost/thread/future.hpp>
#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :

  QMainWindow(parent),
  m_Ui(new Ui::MainWindow),
  m_QmlView(new QQuickView)

{
  m_Ui->setupUi(this);
  m_QmlView->setSource(QUrl(QStringLiteral("qrc:///SimpleQml.qml")));

  auto widget     = m_Ui->frame;
  widget->setLayout(new QVBoxLayout(widget));

  auto container  = QWidget::createWindowContainer(m_QmlView.get(), this);
  container->setMinimumSize(150, 150);
  container->setMaximumSize(1000, 1000);
  container->setSizePolicy(QSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding));

  widget->layout()->addWidget(container);

  m_BoostSignal.connect(
          [this](){
              boost::future<QString> f = boost::async([](){return QString("Hello from  boost future ");});
              this->m_Ui->plainTextEdit->appendPlainText( f.get());
          });

  connect(m_Ui->pushButton, &QPushButton::clicked,
          [this](){
              m_BoostSignal();
          });

  connect(m_QmlView->rootObject(), SIGNAL(qmlSignal(QString)),
          this,                    SLOT(OnQmlSignaled()));
}

void MainWindow::OnQmlSignaled()
{
  m_BoostSignal();
}

MainWindow::~MainWindow() = default;