import QtQuick 2.0

Rectangle {

    id     : main
    visible: true
    width  : 150
    height : 150

    signal qmlSignal(string msg)

    Rectangle {

        Text {
            text: qsTr("Click me")
            anchors.horizontalCenter : parent.horizontalCenter
            anchors.verticalCenter   : parent.verticalCenter
        }

        objectName: "recObjectName"
        id        : rectangle
        width     : 50
        height    : 30
        color     : "skyblue"
        anchors.horizontalCenter : parent.horizontalCenter
        anchors.verticalCenter   : parent.verticalCenter

        states: State {

             name: "rot";
             when: mouseArea.pressed === true
             PropertyChanges { target: rectangle; rotation: 180; color: "red" }
        }

        transitions: Transition {

            from: ""; to: "rot"; reversible: true
            ParallelAnimation {
                NumberAnimation { properties: "rotation"; duration: 500; easing.type: Easing.InOutQuad }
                ColorAnimation  { duration: 500 }
            }
        }

        MouseArea {
            id: mouseArea
            hoverEnabled: true
            anchors.fill: parent

            onReleased: {
                main.qmlSignal("Hello from QML")
            }

            onEntered:  {
                rectangle.color = "lightblue"
            }

            onExited:   {
                rectangle.color = "skyblue"
            }

        }


    }



}
